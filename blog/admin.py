from django.contrib import admin
from .models import Tienda, Producto, Vendedor, Venta, Oferta

admin.site.register(Tienda)
admin.site.register(Producto)
admin.site.register(Vendedor)
admin.site.register(Venta)
admin.site.register(Oferta)


