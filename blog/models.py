from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from django.dispatch import receiver
from allauth.account.signals import user_signed_up

class Post(models.Model):
    author = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    title = models.CharField(max_length=200)
    text = models.TextField()
    created_date = models.DateTimeField(
            default=timezone.now)
    published_date = models.DateTimeField(
            blank=True, null=True)

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __str__(self):
        return self.title

@receiver(user_signed_up)
def create_user_profile(request, user, **kwargs):
    profile = Profile.objects.create(user=user)
    profile.save()

class Usuario(User):
    nombre = models.CharField(max_length=16)
    apellido = models.CharField(max_length=16)

    def __str__(self):
        return self.username


class Tienda(models.Model):
    nombret = models.CharField(max_length=50)
    direccion = models.CharField(max_length=50)
    ciudad = models.CharField(max_length=50)
    comuna = models.CharField(max_length=50)
    telefono = models.CharField(max_length=9)
    encargado = models.CharField(max_length=50)

    def __str__(self):
        return self.nombret

    class Meta:
        verbose_name="Tienda"
        verbose_name_plural="Tiendas"

class Producto(models.Model):
    idp = models.CharField(max_length=10)
    nombrep = models.CharField(max_length=50)
    descripcion = models.CharField(max_length=50)
    precio = models.CharField(max_length=50)
    tipo_producto = models.CharField(max_length=50)

    def __str__(self):
        return self.idp

    class Meta:
        verbose_name="Producto"
        verbose_name_plural="Productos"

class Vendedor(models.Model):
    nombreve = models.CharField(max_length=50)
    Tienda = models.ForeignKey(Tienda, on_delete=models.CASCADE)

    def __str__(self):
        return self.nombreve

    class Meta:
        verbose_name="Vendedor"
        verbose_name_plural="Vendedores"

class Venta(models.Model):
    idv = models.CharField(max_length=50)
    fecha = models.DateTimeField(blank=True, null=True)
    cantidad = models.CharField(max_length=10)
    sucursal = models.CharField(max_length=50)
    Vendedor = models.ForeignKey(Usuario, on_delete=models.CASCADE)

    def __str__(self):
        return self.idv

class Oferta(models.Model):
    ido = models.CharField(max_length=10)
    Producto = models.ForeignKey(Producto, on_delete=models.CASCADE)
    valor_oferta = models.CharField(max_length=4)
    descripcion = models.CharField(max_length=50)

    def __str__(self):
        return self.ido

