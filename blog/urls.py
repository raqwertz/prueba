from django.urls import path
from . import views
from django.contrib.auth import login
from blog import views
from django.contrib.auth import views as auth_views
from django.conf.urls import url
from django.conf.urls import include

urlpatterns = [
    
    path('', views.loginview, name='loginview'),
    path('blog', views.blog, name='blog'),
    path('administrador', views.administradores, name='administrador'),
    path('ListaVendedores', views.ListaVendedores, name='ListaVendedores'),
    path('MantenedorOfertas', views.MantenedorOfertas, name='MantenedorOfertas'),
    path('MantenedorTienda', views.MantenedorTienda, name='MantenedorTienda'),
    path('MantenedorVendedor', views.MantenedorVendedor, name='MantenedorVendedor'),
    path('RegistrarVendedor', views.Registrar_Vendedor, name='RegistrarVendedor'),
    path('Login', views.login_user, name="login"),
    path('RegistrarTienda', views.registrarTienda, name='RegistrarTienda'),
    path('RegistrarOferta', views.registrarOferta, name='RegistrarOferta'),

    # Necesario para allauth
    path('blog/', include('allauth.urls')),
    url('blog', include('social_django.urls', namespace='social')),
   # (r'^blog/$', 'django.contrib.auth.views.login'),

]
