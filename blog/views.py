from django.http import *
from django.shortcuts import render, render_to_response
from django.views.decorators.csrf import csrf_protect
from django.template import RequestContext
from .models import *
from django.utils import timezone
from django.shortcuts import render, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.hashers import *


def loginview(request):
    return render(request, 'blog/IniciarSesion.html', {})
#@login_required
def administradores(request):
    return render(request, 'blog/administrador.html', {})
#@login_required
def administrador(request):
    return render(request, 'blog/administrador.html', {})


#@login_required
def blog(request):
    return render(request, 'blog/blog.html', {})

def ad(request):
    return render(request, 'blog/administrador.html', {})



def ListaVendedores(request):
    vendedores = Usuario.objects.all()
    return render(request, 'blog/listaVendedores.html', {'vendedores': vendedores})

def MantenedorOfertas(request):
    return render(request, 'blog/mantenedorOfertas.html', {})

def MantenedorTienda(request):
    return render(request, 'blog/mantenedorTienda.html', {})

def MantenedorVendedor(request):
    return render(request, 'blog/registroVendedor.html', {})

def Registrar_Vendedor(request):
    if request.method == 'POST':
        if request.POST.get('username') and request.POST.get('password'):
            post = Usuario()
            post.nombre = request.POST.get('nombre')
            post.apellido = request.POST.get('apellido')
            post.username = request.POST.get('username')
            encryptedpassword = make_password(request.POST.get('password'), hasher='default')
            post.password = encryptedpassword
            post.save()
            return render(request, 'blog/IniciarSesion.html')
        else:
            return render(request, 'blog/registroVendedor.html')

@csrf_protect
def login_user(request):
    logout(request)
    username = password = ''
    if request.POST:
        username = request.POST['username']
        password = request.POST['password']
        usuario = authenticate(username=username, password=password)
        if usuario is not None:
            if usuario.is_active:
                if usuario.is_superuser:
                    login(request, usuario)
                    return HttpResponseRedirect('/administrador')
                else:
                    return HttpResponseRedirect('/blog')
    return render(request, 'blog/IniciarSesion.html', {})

def registrarTienda(request):
    if request.method == 'POST':
        if request.POST.get('nombret'):
            post = Tienda()
            post.nombret = request.POST.get('nombret')
            post.direccion = request.POST.get('direccion')
            post.ciudad = request.POST.get('ciudad')
            post.comuna = request.POST.get('comuna')
            post.telefono = request.POST.get('telefono')
            post.encargado = request.POST.get('encargado')
            post.save()
            return render(request, 'blog/mantenedorTienda.html')
        else:
            return render(request, 'blog/mantenedorTienda.html')

def registrarOferta(request):
    if request.method == 'POST':
        if request.POST.get('valoroferta'):
            post = Oferta()
            post.Producto = request.POST.get('producto')
            post.valor_oferta = request.POST.get('valoroferta')
            post.save()
            return render(request, 'blog/mantenedorOfertas.html')
        else:
            return render(request, 'blog/mantenedorOfertas.html')